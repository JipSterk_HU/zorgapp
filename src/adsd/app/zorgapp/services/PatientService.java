package adsd.app.zorgapp.services;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import adsd.app.zorgapp.models.Patient;

/**
 * PatientService
 */
public class PatientService {

    /**
     * instance of the patient service
     * 
     */
    private static PatientService instance;

    /**
     * the filename of the patient file
     */
    private final String fileName = "patients.json";

    /**
     * the list of patients
     */
    private ArrayList<Patient> patients;

    /**
     * the selected patient
     */
    private Patient selectedPatient;

    /**
     * PatientService constructor
     */
    private PatientService() {
        loadPatients();
    }

    /**
     * gets the instance of the patient service
     * 
     * @return the instance of the patient service
     */
    public static PatientService getInstance() {
        if (instance == null) {
            instance = new PatientService();
        }

        return instance;
    }

    /**
     * loads the patient from the file
     * 
     * @return the patient
     */
    private void loadPatients() {
        File file = new File(fileName);

        if (file.exists()) {
            try {
                FileReader fileReader = new FileReader(file);
                JSONObject json = (JSONObject) JSONValue.parse(fileReader);

                ArrayList<Patient> patients = new ArrayList<Patient>();

                Iterator iterator = ((JSONArray) json.get("patients")).iterator();

                while (iterator.hasNext()) {
                    patients.add(Patient.fromJSON((JSONObject) iterator.next()));
                }

                this.patients = patients;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            patients = new ArrayList<Patient>();
        }
    }

    /**
     * saves the patient to a file
     * 
     * @param patient
     */
    private void savePatients() {
        JSONArray patientsJson = new JSONArray();

        for (Patient patient : patients) {
            patientsJson.add(patient.toJSON());
        }

        JSONObject json = new JSONObject();
        json.put("patients", patientsJson);

        try (FileWriter fileWriter = new FileWriter(fileName)) {
            fileWriter.write(json.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * adds a patient
     * 
     * @param patient
     */
    public void addPatient(Patient patient) {
        patients.add(patient);
        savePatients();
    }

    /**
     * removes a patient
     */
    public void removePatient(Patient patient) {
        patients.remove(patient);
        savePatients();
    }

    /**
     * gets a list of patients
     * 
     * @return patients
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * gets the selected patient
     * 
     * @return selectedPatient
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * gets a patient by their first name
     * 
     * @param patient
     */
    public void setSelectPatient(Patient patient) {
        selectedPatient = patient;
    }

    /**
     * try and find a patient by their first name
     * 
     * @param firstName
     * @return
     */
    public Optional<Patient> findPatientByFirstName(String firstName) {
        return patients.stream().filter(patient -> patient.getFirstname().compareTo(firstName) == 0)
                .findFirst();
    }

    /**
     * updates a patient
     * 
     * @param patient
     */
    public void updatePatient(Patient patient) {
        int index = patients.indexOf(patient);

        if (index < 0) {
            System.out.println("Patient not found");
            return;
        }

        patients.set(index, patient);
        savePatients();
    }
}
