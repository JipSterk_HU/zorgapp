package adsd.app.zorgapp.menus;

import adsd.app.zorgapp.Menu;
import adsd.app.zorgapp.User;
import adsd.app.zorgapp.ZorgApp;
import io.github.cdimascio.dotenv.Dotenv;

/**
 * ModeSelectorMenu
 */
public class ModeSelectorMenu implements Menu {

    /**
     * the name of the sub menu
     */
    @Override
    public String getName() {
        return "Mode selector";
    }

    /**
     * is the menu visable
     */
    @Override
    public boolean visable() {
        return true;
    }

    /**
     * the sub menu for mode selector
     */
    @Override
    public void subMenu() {
        int choice = 0;
        do {
            System.out.println("Mode selector");

            System.out.println("Are you a patient or a healthcare provider?");

            System.out.println("0) Patient");
            System.out.println("1) Healthcare provider");
            System.out.println("9) Main menu");

            choice = choice != 9 ? ZorgApp.input.nextInt() : choice;

            switch (choice) {
                case 0:
                    ZorgApp.user = User.PATIENT;
                    System.out.println("Continuing as: Patient");

                    choice = 9;
                    break;
                case 1:
                    System.out.println("To continue as a healthcare provider, please enter the password");
                    String password = ZorgApp.input.next();

                    if (Dotenv.load().get("HEALTHCARE_PROVIDER_PASSWORD").compareTo(password) != 0) {
                        System.out.println("Invalid password");
                        continue;
                    }

                    ZorgApp.user = User.HEALTHCARE_PROVIDER;
                    System.out.println("Continuing as: Healthcare Provider");

                    choice = 9;
                    break;
                case 9:
                    System.out.println("Returning to main menu");
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }

        } while (choice != 9);
    }

}
