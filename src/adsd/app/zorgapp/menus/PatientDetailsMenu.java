package adsd.app.zorgapp.menus;

import java.time.LocalDate;
import java.util.ArrayList;

import adsd.app.zorgapp.Menu;
import adsd.app.zorgapp.ZorgApp;
import adsd.app.zorgapp.models.BMIRegistration;
import adsd.app.zorgapp.models.Medicine;
import adsd.app.zorgapp.models.Patient;
import adsd.app.zorgapp.services.PatientService;

/**
 * PatientDetailsMenu
 */
public class PatientDetailsMenu implements Menu {

    /**
     * the name of the sub menu
     */
    @Override
    public String getName() {
        return "Patient details";
    }

    /**
     * is the menu visable
     */
    @Override
    public boolean visable() {
        return PatientService.getInstance().getSelectedPatient() != null;
    }

    /**
     * the sub menu for patient details
     */
    @Override
    public void subMenu() {
        int choice = 0;
        do {
            System.out.println("Patient details");

            System.out.println("0) Show basic details");
            System.out.println("1) Show medicine details");
            System.out.println("2) Edit basic details");
            System.out.println("3) Show patient bmi registrations");
            System.out.println("9) Main menu");

            choice = ZorgApp.input.nextInt();

            switch (choice) {
                case 0:
                    showBasicPatientDetails();
                    break;
                case 1:
                    showPatientMedicineDetails();
                    break;
                case 2:
                    editBasicPatientDetails();
                    break;
                case 3:
                    showPatientBMIRegistrations();
                    break;
                case 9:
                    System.out.println("Returning to main menu");
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
        } while (choice != 9);
    }

    /**
     * shows basic the details of the patient
     * 
     * @param patient
     * @param choice
     */
    private void showBasicPatientDetails() {
        Patient patient = PatientService.getInstance().getSelectedPatient();

        System.out.println(String.format("surName: %s", patient.getSurname()));
        System.out.println(String.format("firstName: %s", patient.getFirstname()));
        System.out.println(String.format("callname: %s", patient.getCallName()));
        System.out.println(String.format("age: %d", LocalDate.now().getYear() - patient.getDateOfBirth().getYear()));
        System.out.println(String.format("address: %s", patient.getAddress()));
        System.out.println(String.format("length: %.2f", patient.getLength()));
        System.out.println(String.format("weight: %.2f", patient.getWeight()));
        System.out.println(String.format("bmi: %.2f", patient.calcBMI()));
    }

    /**
     * shows the details of the patient's medicine
     */
    private void showPatientMedicineDetails() {
        Patient patient = PatientService.getInstance().getSelectedPatient();
        ArrayList<Medicine> medication = patient.getMedication();

        if (medication.size() == 0) {
            System.out.println(String.format("Patient: %s %s doesn't have medication yet", patient.getFirstname(),
                    patient.getSurname()));
            return;
        }

        for (Medicine medicine : medication) {
            System.out.println("---------------------------------");
            System.out.println(String.format("name: %s:", medicine.getName()));
            System.out.println(String.format("description: %s:", medicine.getDescription()));
            System.out.println(String.format("type: %s:", medicine.getType()));
            System.out.println(String.format("dose: %.2f:", medicine.getDose()));
        }
        System.out.println("---------------------------------");
    }

    /**
     * edit the basic details of a patient
     * 
     */
    private void editBasicPatientDetails() {
        Patient patient = PatientService.getInstance().getSelectedPatient();

        System.out.print("Please enter your new first name:");
        String firstName = ZorgApp.input.next();
        patient.setFirstname(firstName);

        System.out.print("Please enter your new last name:");
        String lastName = ZorgApp.input.next();
        patient.setSurname(lastName);

        System.out.print("Please enter your new call name:");
        String callName = ZorgApp.input.next();
        patient.setCallName(callName);

        System.out.print("Please enter your new age: (yyyy-mm-dd)");
        String age = ZorgApp.input.next();
        patient.setDateOfBirth(LocalDate.parse(age));

        System.out.print("Please enter your new address:");
        ZorgApp.input.nextLine();
        String address = ZorgApp.input.nextLine();
        patient.setAddress(address);

        PatientService.getInstance().updatePatient(patient);

        showBasicPatientDetails();
    }

    /**
     * shows the patient's recent BMI
     */
    private void showPatientBMIRegistrations() {
        Patient patient = PatientService.getInstance().getSelectedPatient();

        ArrayList<BMIRegistration> bmiRegistrations = patient.getBMIRegistrations();

        if (bmiRegistrations.size() == 0) {
            System.out
                    .println(String.format("Patient: %s %s doesn't have BMI registrations yet", patient.getFirstname(),
                            patient.getSurname()));
            return;
        }

        int count = bmiRegistrations.size() >= 10 ? 10 : bmiRegistrations.size();

        bmiRegistrations.sort((left, right) -> right.calculateBMI().intValue() - left.calculateBMI().intValue());

        for (int i = 0; i < count; i++) {
            System.out.println(bmiRegistrations.get(i).toBarChart(bmiRegistrations.get(0).calculateBMI()));
        }
    }
}
