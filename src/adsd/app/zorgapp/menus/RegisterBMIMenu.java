package adsd.app.zorgapp.menus;

import adsd.app.zorgapp.Menu;
import adsd.app.zorgapp.User;
import adsd.app.zorgapp.ZorgApp;
import adsd.app.zorgapp.models.BMIRegistration;
import adsd.app.zorgapp.models.Patient;
import adsd.app.zorgapp.services.PatientService;

/**
 * RegisterBMIMenu
 */
public class RegisterBMIMenu implements Menu {

    /**
     * the name of the sub menu
     */
    @Override
    public String getName() {
        return "Register BMI";
    }

    /**
     * is the menu visable
     */
    @Override
    public boolean visable() {
        return ZorgApp.user == User.HEALTHCARE_PROVIDER && PatientService.getInstance().getSelectedPatient() != null;
    }

    /**
     * the sub menu for patient details
     */
    @Override
    public void subMenu() {
        int choice = 0;
        do {
            System.out.println("Register BMI");

            System.out.println("0) Register BMI for patient");
            System.out.println("9) Main menu");

            choice = ZorgApp.input.nextInt();

            switch (choice) {
                case 0:
                    registerBMIForPatient();
                    break;
                case 9:
                    System.out.println("Returning to main menu");
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
        } while (choice != 9);
    }

    /**
     * register bmi for patient
     */
    private void registerBMIForPatient() {
        Patient patient = PatientService.getInstance().getSelectedPatient();
        System.out.print("Please enter your new length:");
        Double length = ZorgApp.input.nextDouble();
        patient.setLength(length);

        System.out.print("Please enter your new weight:");
        Double weight = ZorgApp.input.nextDouble();
        patient.setWeight(weight);

        BMIRegistration bmiRegistration = new BMIRegistration(weight, length);

        patient.addBMIRegistration(bmiRegistration);

        PatientService.getInstance().updatePatient(patient);
    }
}
