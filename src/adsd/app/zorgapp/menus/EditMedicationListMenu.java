package adsd.app.zorgapp.menus;

import java.util.ArrayList;
import java.util.Optional;

import adsd.app.zorgapp.Menu;
import adsd.app.zorgapp.User;
import adsd.app.zorgapp.ZorgApp;
import adsd.app.zorgapp.models.Medicine;
import adsd.app.zorgapp.models.Patient;
import adsd.app.zorgapp.services.PatientService;

/**
 * EditMedicationListMenu
 */
public class EditMedicationListMenu implements Menu {

    /**
     * the name of the sub menu
     */
    @Override
    public String getName() {
        return "Edit Medication list";
    }

    /**
     * is the menu visable
     */
    @Override
    public boolean visable() {
        return ZorgApp.user == User.HEALTHCARE_PROVIDER;
    }

    /**
     * the sub menu for medication list
     */
    @Override
    public void subMenu() {
        int choice = 0;
        do {
            System.out.println("Medication list");

            System.out.println("0) Show medicine details");
            System.out.println("1) Add new medicine");
            System.out.println("2) Edit medicine");
            System.out.println("3) Remove medicine");
            System.out.println("9) Main menu");

            choice = ZorgApp.input.nextInt();

            switch (choice) {
                case 0:
                    showPatientMedicationDetails();
                    break;
                case 1:
                    addMedicationForPatient();
                    break;
                case 2:
                    editPatientMedicationDetails();
                    break;
                case 3:
                    removeMedicationForPatient();
                    break;
                case 9:
                    System.out.println("Returning to main menu");
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
        } while (choice != 9);
    }

    /**
     * shows the detials of the patient's medicine
     */
    private void showPatientMedicationDetails() {
        Patient patient = PatientService.getInstance().getSelectedPatient();
        ArrayList<Medicine> medication = patient.getMedication();

        if (medication.size() == 0) {
            System.out.println(String.format("Patient: %s %s doesn't have medication yet", patient.getFirstname(),
                    patient.getSurname()));
            return;
        }

        for (Medicine medicine : medication) {
            System.out.println("---------------------------------");
            System.out.println(String.format("name: %s", medicine.getName()));
            System.out.println(String.format("description: %s", medicine.getDescription()));
            System.out.println(String.format("type: %s", medicine.getType()));
            System.out.println(String.format("dose: %.2f", medicine.getDose()));
        }
        System.out.println("---------------------------------");
    }

    /**
     * add medication for a patient
     */
    private void addMedicationForPatient() {
        System.out.print("Please enter the name of the medicine:");
        String name = ZorgApp.input.next();

        System.out.print("Please enter the description of the medicine:");
        ZorgApp.input.nextLine();
        String description = ZorgApp.input.nextLine();

        System.out.print("Please enter the type of the medicine:");
        String type = ZorgApp.input.nextLine();

        System.out.print("Please enter the dose of the medicine:");
        Double dose = ZorgApp.input.nextDouble();

        Medicine medicine = new Medicine(name, description, type, dose);

        Patient patient = PatientService.getInstance().getSelectedPatient();

        patient.addMedication(medicine);

        PatientService.getInstance().updatePatient(patient);
    }

    /**
     * edit a medice for a patient
     */
    private void editPatientMedicationDetails() {
        Patient patient = PatientService.getInstance().getSelectedPatient();

        ArrayList<Medicine> medication = patient.getMedication();

        if (medication.size() == 0) {
            System.out.println(String.format("Patient: %s %s doesn't have medication yet", patient.getFirstname(),
                    patient.getSurname()));
            return;
        }

        System.out.print("What medice would you like to edit:");
        String medicineName = ZorgApp.input.next();

        Optional<Medicine> medicineByName = medication.stream()
                .filter(medicine -> medicine.getName().compareTo(medicineName) == 0)
                .findFirst();

        if (medicineByName.isEmpty()) {
            System.out.println("This patient doens't have that medication");
            return;
        }

        Medicine medicineToEdit = medicineByName.get();

        System.out.print("Please enter the new description:");
        ZorgApp.input.nextLine();
        String description = ZorgApp.input.nextLine();
        medicineToEdit.setDescription(description);

        System.out.print("Please enter the new type:");
        String type = ZorgApp.input.nextLine();
        medicineToEdit.setType(type);

        System.out.print("Please enter the new dose:");
        Double dose = ZorgApp.input.nextDouble();
        medicineToEdit.setDose(dose);

        PatientService.getInstance().updatePatient(patient);
    }

    /**
     * remove medication for a patient
     */
    private void removeMedicationForPatient() {
        Patient patient = PatientService.getInstance().getSelectedPatient();
        ArrayList<Medicine> medication = patient.getMedication();

        if (medication.size() == 0) {
            System.out.println(String.format("Patient: %s %s doesn't have medication yet", patient.getFirstname(),
                    patient.getSurname()));
            return;
        }

        System.out.print("What medice would you like to edit:");
        String medicineName = ZorgApp.input.next();

        Optional<Medicine> medicineByName = medication.stream()
                .filter(medicine -> medicine.getName().compareTo(medicineName) == 0)
                .findFirst();

        if (medicineByName.isEmpty()) {
            System.out.println("This patient doens't have that medication");
            return;
        }

        Medicine medicineToDelete = medicineByName.get();

        patient.removeMedication(medicineToDelete);

        PatientService.getInstance().updatePatient(patient);
    }
}