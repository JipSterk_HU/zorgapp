package adsd.app.zorgapp.menus;

import java.time.LocalDate;
import java.util.Optional;

import adsd.app.zorgapp.Menu;
import adsd.app.zorgapp.ZorgApp;
import adsd.app.zorgapp.models.BMIRegistration;
import adsd.app.zorgapp.models.Patient;
import adsd.app.zorgapp.services.PatientService;

/**
 * PatientSelectorMenu
 */
public class PatientSelectorMenu implements Menu {

    /**
     * the name of the sub menu
     */
    @Override
    public String getName() {
        return "Patient selector";
    }

    /**
     * is the menu visable
     */
    @Override
    public boolean visable() {
        return true;
    }

    /**
     * the sub menu for patient selector
     */
    @Override
    public void subMenu() {
        int choice = 0;
        do {
            System.out.println("Patient selector");

            System.out.println("0) Select patient");
            System.out.println("1) Add patient");
            System.out.println("2) Remove patient");
            System.out.println("9) Main menu");

            choice = ZorgApp.input.nextInt();

            switch (choice) {
                case 0:
                    selectPatient();
                    break;
                case 1:
                    addPatient();
                    break;
                case 2:
                    removePatient();
                    break;
                case 9:
                    System.out.println("Returning to main menu");
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
        } while (choice != 9);
    }

    /**
     * select a patient
     */
    private void selectPatient() {
        System.out.print("What is the first name of the patient would you like to select:");
        String firstName = ZorgApp.input.next();

        Optional<Patient> patient = PatientService.getInstance().findPatientByFirstName(firstName);

        if (patient.isEmpty()) {
            System.out.println("No patient found with that first name");
            return;
        }

        Patient selectedPatient = patient.get();

        PatientService.getInstance().setSelectPatient(selectedPatient);

        System.out.println(
                String.format("Patient %s %s selected", selectedPatient.getFirstname(), selectedPatient.getSurname()));
    }

    /**
     * add a patient
     */
    private void addPatient() {
        Patient patient = new Patient();

        System.out.print("Please enter your first name:");
        String firstName = ZorgApp.input.next();
        patient.setFirstname(firstName);

        System.out.print("Please enter your last name:");
        String lastName = ZorgApp.input.next();
        patient.setSurname(lastName);

        System.out.print("Please enter your call name:");
        String callName = ZorgApp.input.next();
        patient.setCallName(callName);

        System.out.print("Please enter your age: (yyyy-mm-dd)");
        String age = ZorgApp.input.next();
        patient.setDateOfBirth(LocalDate.parse(age));

        System.out.print("Please enter your address:");
        ZorgApp.input.nextLine();
        String address = ZorgApp.input.nextLine();
        patient.setAddress(address);

        System.out.print("Please enter your length:");
        Double length = ZorgApp.input.nextDouble();
        patient.setLength(length);

        System.out.print("Please enter your weight:");
        Double weight = ZorgApp.input.nextDouble();
        patient.setWeight(weight);

        BMIRegistration initialRegistration = new BMIRegistration(weight, length);

        patient.addBMIRegistration(initialRegistration);

        PatientService.getInstance().addPatient(patient);
    }

    /**
     * remove a patient
     */
    private void removePatient() {
        System.out.print("What is the first name of the patient would you like to remove:");
        String firstName = ZorgApp.input.next();

        Optional<Patient> patient = PatientService.getInstance().findPatientByFirstName(firstName);

        if (patient.isEmpty()) {
            System.out.println("No patient found with that first name");
            return;
        }

        PatientService.getInstance().removePatient(patient.get());
    }
}