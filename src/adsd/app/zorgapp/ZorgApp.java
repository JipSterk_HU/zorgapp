package adsd.app.zorgapp;

import java.util.Scanner;

import adsd.app.zorgapp.menus.EditMedicationListMenu;
import adsd.app.zorgapp.menus.ModeSelectorMenu;
import adsd.app.zorgapp.menus.PatientDetailsMenu;
import adsd.app.zorgapp.menus.PatientSelectorMenu;
import adsd.app.zorgapp.menus.RegisterBMIMenu;
import adsd.app.zorgapp.models.Patient;
import adsd.app.zorgapp.services.PatientService;

/**
 * ZorgApp
 */
public class ZorgApp {

	/**
	 * input handler
	 */
	public static Scanner input;

	/**
	 * what type the user is
	 */
	public static User user;

	/**
	 * the menus of the application
	 */
	private Menu[] menus = {
			new ModeSelectorMenu(),
			new PatientSelectorMenu(),
			new PatientDetailsMenu(),
			new RegisterBMIMenu(),
			new EditMedicationListMenu(),
	};

	/**
	 * ZorgApp constructor
	 */
	public ZorgApp() {
		input = new Scanner(System.in);
	}

	/**
	 * start of the application
	 */
	public void run() {
		int choice = 0;
		do {
			System.out.println("ZorgApp");

			Patient selectedPatient = PatientService.getInstance().getSelectedPatient();

			if (selectedPatient != null) {
				System.out.println(String.format("Selected Patient: %s %s", selectedPatient.getFirstname(),
						selectedPatient.getSurname()));
			}

			for (int i = 0; i < menus.length; i++) {
				if (menus[i].visable()) {
					System.out.println(String.format("%d) %s", i, menus[i].getName()));
				}
			}
			System.out.println("9) Exit");

			choice = input.nextInt();

			switch (choice) {
				case 9:
					System.out.println("Goodbye!");
					break;
				default:
					if (choice < menus.length) {
						menus[choice].subMenu();
					} else {
						System.out.println("Invalid choice");
					}
					break;
			}
		} while (choice != 9);
	}

	/**
	 * cleanup function
	 */
	protected void finalize() {
		try {
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
