package adsd.app.zorgapp;

/**
 * User
 */
public enum User {
    /**
     * Patient
     */
    PATIENT,

    /**
     * HealthCare professional
     */
    HEALTHCARE_PROVIDER
}