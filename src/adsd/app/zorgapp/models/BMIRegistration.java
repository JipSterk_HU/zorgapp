package adsd.app.zorgapp.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.json.simple.JSONObject;

/**
 * BMIRegistration
 */
public class BMIRegistration {

    /**
     * the date of the bmi registration
     */
    private LocalDate dateOfRegistration;

    /**
     * the weight of the patient
     */
    private Double weight;

    /**
     * the lenght of the patient
     */
    private Double length;

    /**
     * BMIRegistration constructor
     * 
     * @param weight
     * @param length
     */
    public BMIRegistration(Double weight, Double length) {
        this.dateOfRegistration = LocalDate.now();
        this.weight = weight;
        this.length = length;
    }

    /**
     * BMIRegistration constructor
     * 
     * @param dateOfRegistration
     * @param weight
     * @param length
     */
    private BMIRegistration(LocalDate dateOfRegistration, Double weight, Double length) {
        this.dateOfRegistration = dateOfRegistration;
        this.weight = weight;
        this.length = length;
    }

    /**
     * calculate bmi for registration
     */
    public Double calculateBMI() {
        return this.weight / (this.length * this.length);
    }

    /**
     * calculate the length of the bar
     * 
     * @param highestBMI
     * 
     * @return
     */
    private String calculateBar(Double highestBMI) {
        int length = (int) (calculateBMI() * (100 / highestBMI));

        StringBuilder bar = new StringBuilder();

        for (int i = 0; i <= length; i++) {
            bar.append("*");
        }

        return bar.toString();
    }

    /**
     * bmi registration to json
     * 
     * @return
     */
    public JSONObject toJSON() {
        return new JSONObject() {
            {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
                String formattedDate = dateOfRegistration.format(formatter);
                put("dateOfRegistration", formattedDate);
                put("weight", weight);
                put("length", length);
            }
        };
    }

    /**
     * creates a bmi registration from json
     * 
     * @param json
     * @return
     */
    public static BMIRegistration fromJSON(JSONObject json) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
        LocalDate dateOfRegistration = LocalDate.parse((String) json.get("dateOfRegistration"), formatter);
        Double weight = (Double) json.get("weight");
        Double length = (Double) json.get("length");

        return new BMIRegistration(dateOfRegistration, weight, length);
    }

    /**
     * string overloader for bmi registration
     */
    public String toBarChart(Double highestBMI) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
        String formattedDate = dateOfRegistration.format(formatter);

        return String.format("[%s] %s (%.2f kg | %.2f bmi)", formattedDate, calculateBar(highestBMI), weight,
                calculateBMI());
    }

}
