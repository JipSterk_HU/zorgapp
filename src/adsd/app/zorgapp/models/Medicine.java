package adsd.app.zorgapp.models;

import org.json.simple.JSONObject;

/**
 * Medicine
 */
public class Medicine {
    /**
     * the name of the medicine
     */
    private String name;

    /**
     * the description of the medicine
     */
    private String description;

    /**
     * the type of the medicine
     */
    private String type;

    /**
     * the dose of the medicine
     */
    private Double dose;

    /**
     * medice constructor
     */
    public Medicine() {
        this.name = "";
        this.description = "";
        this.type = "";
        this.dose = 0.0;
    }

    /**
     * medice constructor
     * 
     * @param name
     * @param description
     * @param type
     * @param dose
     */
    public Medicine(String name, String description, String type, Double dose) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.dose = dose;
    }

    /**
     * gets the name of the medicine
     */
    public String getName() {
        return name;
    }

    /**
     * sets the name of the medicine
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * gets the description of the medicine
     */
    public String getDescription() {
        return description;
    }

    /**
     * sets the description of the medicine
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * gets the type of the medicine
     * 
     * @return dose
     */
    public String getType() {
        return type;
    }

    /**
     * sets the type of the medicine
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * gets the dose of the medicine
     * 
     * @return dose
     */
    public Double getDose() {
        return dose;
    }

    /**
     * sets the dose of the medicine
     * 
     * @param dose
     */
    public void setDose(Double dose) {
        this.dose = dose;
    }

    /**
     * medicine to json
     * 
     * @return json
     */
    public JSONObject toJSON() {
        return new JSONObject() {
            {
                put("name", name);
                put("description", description);
                put("type", type);
                put("dose", dose);
            }
        };
    }

    /**
     * creates a medicine from json
     * 
     * @param json
     * @return medice
     */
    public static Medicine fromJSON(JSONObject json) {
        String name = (String) json.get("name");
        String description = (String) json.get("description");
        String type = (String) json.get("type");
        Double dose = (Double) json.get("dose");

        return new Medicine(name, description, type, dose);
    }
}
