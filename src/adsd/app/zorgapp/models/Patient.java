package adsd.app.zorgapp.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * A patient
 */
public class Patient {

	/**
	 * the sur name of the patient
	 */
	private String surName;

	/**
	 * the first name of the patient
	 */
	private String firstName;

	/**
	 * the name to call the patient
	 */
	private String callName;

	/**
	 * the address of the patient
	 */
	private String address;

	/**
	 * the date the patient was born
	 */
	private LocalDate dateOfBirth;

	/**
	 * the weight of the patient
	 */
	private Double weight = 0.0;

	/**
	 * the length of the patient
	 */
	private Double length = 0.0;

	/**
	 * the list of medicines
	 */
	private ArrayList<Medicine> medicines;

	/**
	 * the list of bmi registrations
	 */
	private ArrayList<BMIRegistration> bmiRegistrations;

	/**
	 * patient constructor
	 */
	public Patient() {
		this.surName = "";
		this.firstName = "";
		this.callName = "";
		this.address = "";
		this.dateOfBirth = LocalDate.now();
		this.medicines = new ArrayList<Medicine>();
		this.bmiRegistrations = new ArrayList<BMIRegistration>();
	}

	/**
	 * patient constructor
	 * 
	 * @param surName
	 * @param firstName
	 * @param callName
	 * @param address
	 * @param dateOfBirth
	 * @param weight
	 * @param length
	 * @param medicines
	 * @param bmiRegistrations
	 */
	public Patient(String surName, String firstName, String callName, String address, LocalDate dateOfBirth,
			Double weight, Double length, ArrayList<Medicine> medicines, ArrayList<BMIRegistration> bmiRegistrations) {
		this.surName = surName;
		this.firstName = firstName;
		this.callName = callName;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.weight = weight;
		this.length = length;
		this.medicines = medicines;
		this.bmiRegistrations = bmiRegistrations;
	}

	/**
	 * gets the surName of a patient
	 */
	public String getSurname() {
		return surName;
	}

	/**
	 * sets the surName of a patient
	 * 
	 * @param surName
	 */
	public void setSurname(String surName) {
		this.surName = surName;
	}

	/**
	 * gets the first name of a patient
	 */
	public String getFirstname() {
		return firstName;
	}

	/**
	 * sets the first name of a patient
	 * 
	 * @param firstName
	 */
	public void setFirstname(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * gets the call name of a patient
	 */
	public String getCallName() {
		return callName;
	}

	/**
	 * sets the call name of a patient
	 * 
	 * @param callName
	 */
	public void setCallName(String callName) {
		this.callName = callName;
	}

	/**
	 * gets the address of a patient
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * sets the address of a patient
	 * 
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * gets the date of birth of a patient
	 */
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * sets the date of birth of a patient
	 * 
	 * @param dateOfBirth
	 */
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * gets the weight of a patient
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * sets the weight of a patient
	 * 
	 * @param weight
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	/**
	 * gets the length of a patient
	 */
	public Double getLength() {
		return length;
	}

	/**
	 * sets the length of a patient
	 * 
	 * @param length
	 */
	public void setLength(Double length) {
		this.length = length;
	}

	/**
	 * calculates the BMI of a patient
	 */
	public Double calcBMI() {
		return weight / (length * length);
	}

	/**
	 * gets the list of medicines of a patient
	 * 
	 * @return medicines
	 */
	public ArrayList<Medicine> getMedication() {
		return medicines;
	}

	/**
	 * add a madicine to the list
	 * 
	 * @param medicine
	 */
	public void addMedication(Medicine medicine) {
		medicines.add(medicine);
	}

	/**
	 * remove a medicine form the list
	 * 
	 * @param medicine
	 */
	public void removeMedication(Medicine medicine) {
		medicines.remove(medicine);
	}

	/**
	 * gets the list of bmi registrations of a patient
	 * 
	 * @return bmiRegistrations
	 */
	public ArrayList<BMIRegistration> getBMIRegistrations() {
		return bmiRegistrations;
	}

	/**
	 * add a bmi registration to the list
	 * 
	 * @param bmiRegistration
	 */
	public void addBMIRegistration(BMIRegistration bmiRegistration) {
		bmiRegistrations.add(bmiRegistration);
	}

	/**
	 * patient to json
	 * 
	 * @return json
	 */
	public JSONObject toJSON() {
		return new JSONObject() {
			{
				put("surName", surName);
				put("firstName", firstName);
				put("callName", callName);
				put("address", address);
				put("dateOfBirth", dateOfBirth.toString());
				put("weight", weight);
				put("length", length);

				JSONArray medicines = new JSONArray();

				for (Medicine medicine : Patient.this.medicines) {
					medicines.add(medicine.toJSON());
				}

				put("medicines", medicines);

				JSONArray bmiRegistrations = new JSONArray();

				for (BMIRegistration bmiRegistration : Patient.this.bmiRegistrations) {
					bmiRegistrations.add(bmiRegistration.toJSON());
				}

				put("bmiRegistrations", bmiRegistrations);
			}
		};
	}

	/**
	 * creates a patient from json
	 * 
	 * @param json
	 * @return patient
	 */
	public static Patient fromJSON(JSONObject json) {
		String surName = (String) json.get("surName");
		String firstName = (String) json.get("firstName");
		String callName = (String) json.get("callName");
		String address = (String) json.get("address");
		LocalDate dateofBirth = LocalDate.parse((String) json.get("dateOfBirth"));
		Double weight = (Double) json.get("weight");
		Double length = (Double) json.get("length");
		ArrayList<Medicine> medicines = new ArrayList<Medicine>();

		Iterator medicinesIterator = ((JSONArray) json.get("medicines")).iterator();

		while (medicinesIterator.hasNext()) {
			medicines.add(Medicine.fromJSON((JSONObject) medicinesIterator.next()));
		}

		ArrayList<BMIRegistration> bmiRegistrations = new ArrayList<BMIRegistration>();

		Iterator bmiRegistrationsIterator = ((JSONArray) json.get("bmiRegistrations")).iterator();

		while (bmiRegistrationsIterator.hasNext()) {
			bmiRegistrations.add(BMIRegistration.fromJSON((JSONObject) bmiRegistrationsIterator.next()));
		}

		return new Patient(surName, firstName, callName, address, dateofBirth, weight, length, medicines,
				bmiRegistrations);
	}
}
