package adsd.app.zorgapp;

/**
 * Menu
 */
public interface Menu {
    /**
     * the name of the menu
     * 
     * @return the name of the menu
     */
    public String getName();

    /**
     * is the submenu visable
     */
    public boolean visable();

    /**
     * the sub menu
     */
    public void subMenu();
}