package adsd.app.zorgapp;

/**
 * App
 */
public class App {

    /**
     * start of the application
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        new ZorgApp().run();
    }
}
