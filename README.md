## Getting Started

copy over the [.env.example](./.env.example) to `.env` and choose a password

## Running the Application

press <kbd>F5</kbd> to run the application in vscode
